#-------------------------------------------------------------------------------
# Name:        main.py
# Purpose:     Main entry point for the VERS Character Creator
#
# Author:      Clay Parker
#
# Created:     17/10/2021
# Copyright:   (c) Clay Parker 2021
# Licence:     <To Be Decided!>
#-------------------------------------------------------------------------------

from src.server import Server

def main():
    new_server = Server()
    new_server.process()

if __name__ == '__main__':
    main()
