#-------------------------------------------------------------------------------
# Name:        Server
# Purpose:     Main app server for the VERS Character Creator
#
# Author:      Clay Parker
#
# Created:     17/10/2021
# Copyright:   (c) Clay Parker 2021
# Licence:     <To Be Decided!>
#-------------------------------------------------------------------------------

import os
from http.server import HTTPServer, CGIHTTPRequestHandler

class Server:

    def __init__(self):
        pass

    def process(self):
        # Change to the correct directory
        curdir = os.getcwd()
        os.chdir('resources')
        # Create the server object listening to port 80
        server_object = HTTPServer(server_address=('',80),
            RequestHandlerClass = CGIHTTPRequestHandler)
        # Start the server
        server_object.serve_forever()

